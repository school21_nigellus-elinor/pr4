# GIT

## git bash 

git bash - приложение для сред Microsoft Windows, эмулирующее работу командно строки Git.

## git pull 

git pull - команда, позволяющая скачать и применить изменения из удалённого репозитория 

GitLab -> копьютер

## git push

git push - команда, позволяющая загрузить изменения с локального репозитория на удалённый 

компьютер -> GitLab

## merge request

merge request - запрос от пользователя на слияние двух веток (применение изменений одной ветки к другой)

## git status

git status - команда, позволяющая посмотреть состояние рабочей директории в данный момент. например, в какие файлы были внесены изменения, на какой ветке мы сейчас находимся 

## git log 

git log - команда позволяющая посмотреть историю коммитов 

## submodule

submodule - инструмент гита, позволяющий хранить один Git-репозиторий как подкаталог другого репозитория